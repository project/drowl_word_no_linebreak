<?php

/**
 * @file
 */

/**
 * drowl_word_no_linebreak.admin settings form.
 */
function drowl_word_no_linebreak_settings_form() {
  $module_path = drupal_get_path('module', 'drowl_word_no_linebreak');

  $form['drowl_word_no_linebreak_replacements'] = array(
    '#type' => 'textfield',
    '#title' => 'Wrap following words into &lt;span class="no-linebreak"&gt;&lt;/span&gt;',
    '#description' => t(
        "Enter words comma separated to rap following words into &lt;span class='no-linebreak'&gt;&lt;/span&gt; dynamically at runtime."),
    '#required' => true,
    '#default_value' => variable_get('drowl_word_no_linebreak_replacements', ''));

  return system_settings_form($form);
}
